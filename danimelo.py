#!/usr/bin/env python3
'''
$pylint codewars.py
-------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 9.38/10, +0.62)

$ mypy danimelo.py
Success: no issues found in 1 source file

'''
from typing import List


def order(sentence: str) -> str:

    """ Split the sentence -> return the ordered string """
    get_new = sorting(sentence.split())

    return " ".join(get_new)


def sorting(splited_sentence: List[str]) -> List[str]:

    """ Return or_sentence in the right order """
    lets_order = []
    for i in range(1, 10):  # starting in zero(0) is not valid, so just (1-9)
        for item in splited_sentence:
            if str(i) in item:
                lets_order.append(item)  # numerical order done
    return lets_order


def main() -> None:

    """ Read the data """
    with open("DATA.lst") as file:
        data = file.read()
        print(order(data))


if __name__ == "__main__":

    main()

#
# $ ./danimelo.py
# Thi1s is2 3a T4est
# $
